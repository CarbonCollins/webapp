const helloController = {
  async helloWorld(ctx) {
    ctx.body = 'Hello World!';
  }
}

export default helloController;
