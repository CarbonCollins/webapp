const logService = {
  debug(...data) {
    console.debug('[DEBUG]', ...data);
  },
  info(...data) {
    console.info('[INFO]', ...data);
  },
  warn(...data) {
    console.warn('[WARN]', ...data);
  },
  error(...data) {
    console.error('[ERROR]', ...data);
  }
};

export default logService;
