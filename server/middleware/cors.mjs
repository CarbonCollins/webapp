import cors from '@koa/cors';

export default cors({
  allowMethods: ['GET'],
  credentials: false
});
