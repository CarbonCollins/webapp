import Router from 'koa-router';

import helloController from '../controllers/hello.mjs';

const router = new Router();

router
  .get('/api/hello', helloController.helloWorld);

export default router;
