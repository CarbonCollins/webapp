import Koa from 'koa';

import log from './services/log.mjs';

import compress from './middleware/compress.mjs';
import cors from './middleware/cors.mjs';

let server = null;
const port = process.env?.PORT ?? '5000';
const app = new Koa();

function closeHandler(err) {
  if (err) {
    log.error(err);
    process.exit(1);
  }

  log.info('Server closed.');
  process.exit(0);
}

function handleSignal(signal) {
  log.debug(`"${signal}" signal received, closing server...`);

  if (server?.close) {
    server.close(closeHandler);
  } else {
    closeHandler();
  }
}

async function handleFallback(ctx) {
  ctx.status = 404;
}

async function handleOriginAndProtocolRedirects(ctx, next) {
  const originalHref = ctx.request.href;
  let href = originalHref;

  if (href.startsWith('http://')) href = href.replace('http://', 'https://');
  if (href.includes('www.')) href = href.replace('www.', '');
  if (href.includes('carboncollins.uk')) href = href.replace('carboncollins.uk', 'carboncollins.se');

  if (originalHref !== href) {
    ctx.status = 301;
    ctx.redirect(href);

    return;
  }

  await next();
}

app
  .use(cors)
  .use(compress)
  .use(handleOriginAndProtocolRedirects)
  .use(router.routes())
  .use(router.allowedMethods())
  .use(handleFallback);

server = app.listen(port, () => {
  log.debug('Server port:', port);
  log.info('Server started.');
});

process.on('SIGINT', handleSignal);
process.on('SIGTERM', handleSignal);
process.on('SIGHUP', handleSignal);
