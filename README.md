# CarbonCollins Application

A [Vue 3](https://v3.vuejs.org/) application for the [https://carboncollins.se](https://carboncollins.se) and [https://carboncollins.uk](https://carboncollins.uk) domains.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build:production
```

### Lints and fixes files
```
npm run lint
```
