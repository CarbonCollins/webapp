import { createI18n } from 'vue-i18n';
import {
  language as enGbLanguage,
  messages as enGbMessages,
  numbers as enGbNumbers,
  dateTime as enGBDateTime
} from './locales/enGB';


type MessageSchema = typeof enGbMessages;
type NumberFormats = typeof enGbNumbers;
type DateTimeFormats = typeof enGBDateTime;
type I18nSchemas = { message: MessageSchema, number: NumberFormats, datetime: DateTimeFormats };

export const i18n = createI18n<I18nSchemas, typeof enGbLanguage>({
  legacy: false,
  allowComposition: true,
  locale: enGbLanguage,
  fallbackLocale: {
    [enGbLanguage]: [],
    default: [enGbLanguage]
  },
  missingWarn: /^(?!common)/,
  fallbackWarn: /^(?!common)/,
  availableLocales: [
    enGbLanguage
  ],
  messages: {
    [enGbLanguage]: enGbMessages
  },
  numberFormats: {
    [enGbLanguage]: enGbNumbers
  },
  datetimeFormats: {
    [enGbLanguage]: enGBDateTime
  }
});
