import { RouteLocationNormalized, createRouter, createWebHistory } from 'vue-router';

import { filterValidProjectRoutes } from './composables/useProjectVersions';
import { homeLabProjectRoutes } from './views/projects/homeLab/routes';
import { plattform33ProjectRoutes } from './views/projects/plattform33/routes';
import { breakGlassProjectRoutes } from './views/projects/breakGlass/routes';
import { terminusCentralProjectRoutes } from './views/projects/terminusCentral/routes';

declare module 'vue-router' {
  interface RouteMeta {
    project?: boolean;
    version?: string;
    title?: string;
    tags?: string[];
    description?: string;
    hidden?: boolean;
  }
}

export const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('./views/HomeView.vue')
    },
    {
      path: '/about',
      name: 'about',
      component: () => import('./views/AboutView.vue'),
      meta: {
        title: 'About'
      }
    },
    homeLabProjectRoutes,
    plattform33ProjectRoutes,
    breakGlassProjectRoutes,
    terminusCentralProjectRoutes,
    {
      path: '/projects',
      name: 'projects',
      component: () => import('./views/ProjectsView.vue'),
      meta: {
        title: 'Projects'
      }
    },
    {
      path: '/:pathMatch(.*)*',
      redirect: {
        name: 'home'
      }
    }
  ], 
});

const baseDomain = 'https://carboncollins.se';
const defaultTitle = 'Steven Collins';
const defaultDescription = 'A web app for Steven Collins';
const defaultTags = ['steven', 'collins'];
const excludedTags = ['active', 'inactive', 'archived', 'on-pause', 'deprecated', 'abandoned'];

function removeMetaTagWithSelector(selector: string) {
  Array.from(document.querySelectorAll(selector))
    .map(el => el?.parentNode?.removeChild(el));

}

function updateCanonicalMeta(to: RouteLocationNormalized) {
  removeMetaTagWithSelector('[rel="canonical"]');

  const canonicalPath = to.path === '/' ? '' : to.path;
  const canonicalMeta = document.createElement('meta');
  canonicalMeta.setAttribute('rel', 'canonical');
  canonicalMeta.setAttribute('href', `${baseDomain}${canonicalPath}`);
  document.head.appendChild(canonicalMeta);
}

function updateTagsMeta(to: RouteLocationNormalized) {
  removeMetaTagWithSelector('[name="keywords"]');

  const keywords = new Set(
    [
      ...defaultTags,
      ...(to?.meta?.tags ?? []),
      ...((to.meta.title ?? '').split(' '))
    ]
    .map(keyword => keyword.toLocaleLowerCase().trim())
    .filter(keyword => keyword !== null && keyword !== '')
    .filter(keyword => !excludedTags.includes(keyword))
  );

  const keywordsMeta = document.createElement('meta');
  keywordsMeta.setAttribute('name', 'keywords');
  keywordsMeta.setAttribute('content', Array.from(keywords).join(','));
  document.head.appendChild(keywordsMeta);
}

function updateDescriptionMeta(to: RouteLocationNormalized) {
  removeMetaTagWithSelector('[name="description"]');
  const description = to.meta?.description ?? defaultDescription;

  const descriptionMeta = document.createElement('meta');
  descriptionMeta.setAttribute('name', 'description');
  descriptionMeta.setAttribute('content', description);
  document.head.appendChild(descriptionMeta);
}

function updateTitle(to: RouteLocationNormalized) {
  if (to.meta?.title) {
    document.title = `${to.meta.title} | ${defaultTitle}`;
  } else {
    document.title = defaultTitle;
  }
}

function updateSeriesMeta(to: RouteLocationNormalized) {
  removeMetaTagWithSelector('[rel="next"]');
  removeMetaTagWithSelector('[rel="prev"]');

  const projectVersions = filterValidProjectRoutes(router.options.routes, to.name);
  const currentVersionIndex = projectVersions.findIndex(version => version.current);

  if (projectVersions.length > 1 && currentVersionIndex >= 0) {

    const nextVersionPath = projectVersions?.[currentVersionIndex + 1]?.path;
    if (currentVersionIndex < (projectVersions.length - 1) && nextVersionPath) {
      const nextMeta = document.createElement('meta');
      nextMeta.setAttribute('rel', 'next');
      nextMeta.setAttribute('href', `${baseDomain}${nextVersionPath}`);
      document.head.appendChild(nextMeta);
    }

    const prevVersionPath = projectVersions?.[currentVersionIndex - 1]?.path;
    if (currentVersionIndex > 0 && prevVersionPath) {
      const prevMeta = document.createElement('meta');
      prevMeta.setAttribute('rel', 'prev');
      prevMeta.setAttribute('href', `${baseDomain}${prevVersionPath}`);
      document.head.appendChild(prevMeta);
    }
  }
}

router.beforeResolve((to: RouteLocationNormalized) => {
  updateTitle(to);
  updateDescriptionMeta(to);
  updateCanonicalMeta(to);
  updateTagsMeta(to);
  updateSeriesMeta(to);
});
