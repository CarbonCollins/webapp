import { ProjectRouteRecord } from "../../../types/projectRoutes";

export const plattform33ProjectRoutes: ProjectRouteRecord<"plattform33", 1> = {
  path: "/projects/plattform33",
  meta: {
    project: true,
  },
  children: [
    {
      path: "v1",
      name: "plattform33-v1",
      component: () => import("./Plattform33V1.vue"),
      meta: {
        title: "Plattform33",
        version: "v1",
        tags: ["vue", "train", "gcp", "experimental", "active"],
      },
    },
    {
      path: "/:pathMatch(.*)*",
      name: "plattform33",
      redirect: { name: "plattform33-v1" },
    },
  ],
};
