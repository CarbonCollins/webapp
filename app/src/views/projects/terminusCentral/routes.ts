import { ProjectRouteRecord } from "../../../types/projectRoutes";

export const terminusCentralProjectRoutes: ProjectRouteRecord<
  "terminus-central",
  1
> = {
  path: "/projects/terminus-central",
  meta: {
    project: true,
  },
  children: [
    {
      path: "v1",
      name: "terminus-central-v1",
      component: () => import("./TerminusCentralV1.vue"),
      meta: {
        title: "Terminus Central",
        version: "v1",
        tags: ["php", "archived"],
      },
    },
    {
      path: "/:pathMatch(.*)*",
      name: "terminus-central",
      redirect: { name: "terminus-central-v1" },
    },
  ],
};
