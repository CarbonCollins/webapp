import { ProjectRouteRecord } from '../../../types/projectRoutes';

export const homeLabProjectRoutes: ProjectRouteRecord<'homelab', 1 | 2 | 3 | 4 | 5 | 6> = {
  path: '/projects/homelab',
  meta: {
    project: true
  },
  children: [
    {
      path: 'v1',
      name: 'homelab-v1',
      component: () => import('./HomeLabV1.vue'),
      meta: {
        title: 'Home Lab',
        version: 'v1',
        tags: [
          'proxmox',
          'homelab'
        ]
      }
    },
    {
      path: 'v2',
      name: 'homelab-v2',
      component: () => import('./HomeLabV2.vue'),
      meta: {
        title: 'Home Lab',
        version: 'v2',
        tags: [
          'proxmox',
          'homelab'
        ]
      }
    },
    {
      path: 'v3',
      name: 'homelab-v3',
      component: () => import('./HomeLabV3.vue'),
      meta: {
        title: 'Home Lab',
        version: 'v3',
        tags: [
          'proxmox',
          'terraform',
          'homelab'
        ]
      }
    },
    {
      path: 'v4',
      name: 'homelab-v4',
      component: () => import('./HomeLabV4.vue'),
      meta: {
        title: 'Home Lab',
        version: 'v4',
        tags: [
          'cloud',
          'nomad',
          'consul',
          'vault',
          'terrafrom',
          'ansible',
          'homelab'
        ]
      }
    },
    {
      path: 'v5',
      name: 'homelab-v5',
      component: () => import('./HomeLabV5.vue'),
      meta: {
        title: 'Home Lab',
        version: 'v5',
        tags: [
          'cloud',
          'nomad',
          'consul',
          'vault',
          'terrafrom',
          'ansible',
          'homelab'
        ]
      }
    },
    {
      path: 'v6',
      name: 'homelab-v6',
      component: () => import('./HomeLabV6.vue'),
      meta: {
        title: 'Home Lab',
        version: 'v6',
        tags: [
          'cloud',
          'kubernetes',
          'opentofu',
          'ansible',
          'homelab',
          'active'
        ]
      }
    },
    { path: '/:pathMatch(.*)*', name: 'homelab', redirect: { name: 'homelab-v6' } }
  ]
};
