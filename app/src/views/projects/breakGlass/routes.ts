import { ProjectRouteRecord } from "../../../types/projectRoutes";

export const breakGlassProjectRoutes: ProjectRouteRecord<"break-glass", 1> = {
  path: "/projects/break-glass",
  meta: {
    project: true,
  },
  children: [
    {
      path: "v1",
      name: "break-glass-v1",
      component: () => import("./BreakGlassV1.vue"),
      meta: {
        title: "Break Glass",
        version: "v1",
        tags: ["active"],
      },
    },
    {
      path: "/:pathMatch(.*)*",
      name: "break-glass",
      redirect: { name: "break-glass-v1" },
    },
  ],
};
