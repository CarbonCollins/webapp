import type { RouteComponent, RouteRecordRaw } from 'vue-router';

type VersionString<Num extends number = number> = `v${Num}`

export interface ProjectVersionRouteRecord<
  Name extends string,
  Num extends number
> extends RouteRecordRaw {
  path: VersionString<Num>,
  name: `${string}-${VersionString<Num>}`,
  component: () => Promise<RouteComponent>,
  meta: {
    title: string,
    version: VersionString<Num>,
    tags: string[]
  }
}

export interface ProjectVersionRedirect<Name extends string, Num extends number> {
  path: '/:pathMatch(.*)*';
  name: Name,
  redirect: {
    name: `${Name}-${VersionString<Num>}`
  };
}

export interface ProjectRouteRecord<Name extends string, Versions extends number> extends RouteRecordRaw {
  path: `/projects/${string}`,
  meta: {
    project: true
  };
  children: Array<ProjectVersionRedirect<Name, Versions> | ProjectVersionRouteRecord<Name, Versions>>
}
