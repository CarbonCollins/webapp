import { IntlDateTimeFormat } from 'vue-i18n';

export const language = 'en-GB';

export const messages = {
  common: {
    name: {
      first: 'Steven',
      last: 'Collins',
      full: '@:common.name.first @:common.name.last',
    },
    profile: 'profile',
    header: {
      aim: 'Aim',
      status: 'Status',
      description: 'Description',
      techStack: 'Tech Stack',
      hardware: 'Hardware',
      linkedProjects: 'Linked Projects',
      externalLinks: 'External Links'
    }
  },
  project: {
    homelab: {
      name: 'Home Lab',
      description: 'A self hosted cloud for hosting services'
    },
    plattform33: {
      name: 'Plattform33',
      description: 'Tracks trains within Skåne (Sweden) to know where on a platform to stand'
    },
    breakGlass: {
      name: 'Break Glass',
      description: 'A step by step guide to follow when decommission things after I am unable to do so anymore'
    },
    terminusCentral: {
      name: 'Terminus Central',
      description: 'A PHP application that allowed friends to request game servers and to stream media files to me.'
    },
    header: {
      aim: 'Aim'
    }
  },
  view: {
    projects: {
      name: 'Projects',
      title: `A listing of Steven's projects`,
      header: 'An archive of the projects I have worked on.'
    },
    blog: {
      name: 'Blog',
      title: 'A blog written by Steven',
      header: 'A blog or a stream of conscience whichever I feel like at the time',
      entries: {
        header: 'Blog Entries',
        title: '#{number} - {title}',
        postedBy: 'Posted {datetime} by {author}'
      }
    },
    about: {
      name: 'About',
      title: 'Link to a page about @:common.name.first',
      header: 'A technology enthusiast at heart and a maker or breaker of things in general.',
      professional: {
        header: 'What I do professionally',
        body: 'I work as a software developer at {alstom} focused on (but not limited to) web and cloud spaces. In the current project I am working on, I work mainly with {vue} and {azure} while also handling some {java} and {springboot}. While I have worked in other web and cloud focused jobs roles, originally I came from an electronics and testing background where I designed, built, and programmed automation jigs for testing manufactured electronics using {labview}.'
      },
      personal: {
        header: 'What I do personally',
        body: 'At home I mess with anything that takes my fancy! As of the time of writing this I am currently playing with home automation using {homeassistant} and rebuilding a {kubernetes} cluster stack which runs various containerised services for me and the people I live with. I also dabble with hardware including {raspberrypi} and {esp32} devices to provide extra functionality within the house.'
      },
      find: {
        header: 'Where to find me',
        body: 'You can find me in a few different places online. If your looking for the source code for my projects then go to my {gitlab} If published code is more your thing then check me out on my {npm} or if you are wanting to look at something more presentable then my {linkedin}!'
      }
    }
  },
  software: {
    gitlab: {
      name: 'GitLab',
      profileName: '@:software.gitlab.name @:common.profile',
      profileLinkTitle: 'Link to the GitLab profile page for @:common.name.first'
    },
    npm: {
      name: 'NPM',
      profileName: '@:software.npm.name @:common.profile',
      profileLinkTitle: 'Link to the NPM profile page for @:common.name.first'
    },
    linkedIn: {
      name: 'LinkedIn',
      profileName: '@:software.linkedIn.name @:common.profile',
      profileLinkTitle: 'Link to the LinkedIn profile page for @:common.name.first'
    },
    vue: {
      name: 'Vue.JS',
      homepageLinkTitle: 'Home page for Vue.JS'
    },
    azureCloud: {
      name: 'Azure Cloud',
      homepageLinkTitle: 'Home page for Microsoft Azure Cloud'
    },
    java: {
      name: 'Java',
      homepageLinkTitle: 'Home page for Java'
    },
    springBoot: {
      name: 'Spring Boot',
      homepageLinkTitle: 'The project page for Spring Boot on the Spring website'
    },
    labView: {
      name: 'LabView',
      homepageLinkTitle: 'The project page for LabView on the National Instruments website'
    },
    homeAssistant: {
      name: 'Home Assistant',
      homepageLinkTitle: 'The homepage for Home Assistant'
    },
    proxmox: {
      name: 'Proxmox',
      description: 'Linux based hypervisor for VMs and LXC containers',
      homepageLinkTitle: 'The homepage for Proxmox'
    },
    kubernetes: {
      name: 'Kubernetes',
      homepageLinkTitle: 'The homepage for Kubernetes'
    },
    terraform: {
      name: 'HashiCorp Terraform',
      description: 'Infrastructure as Code provided by HashiCorp',
      homepageLinkTitle: 'The homepage for Terraform',
      productPageLinkTitle: 'The product page for Hashicorp Terraform'
    },
    lxc: {
      name: 'LXC',
      description: 'Userspace interface for the Linux kernel containment features',
      homepageLinkTitle: 'The homepage for Linux Containers',
      productPageLinkTitle: 'The product page for LXC'
    }
  },
  hardware: {
    raspberryPi: {
      homepageLinkTitle: 'The homepage for the Raspberry Pi',
      general: {
        single: 'Raspberry Pi',
        plural: 'Raspberry Pis',
        count: '{count} Raspberry Pis | {count} Raspberry Pi | {count} Raspberry Pis',
      },
      raspi2b: {
        single: 'Raspberry Pi 2 Model B',
        description: 'ARM based Single Board Computer'
      },
      raspi3bplus: {
        single: 'Raspberry Pi 3 Model B+',
        description: 'ARM based Single Board Computer'
      },
      raspi4b2gb: {
        single: 'Raspberry Pi 4 Model B (2GB)',
        description: 'ARM based Single Board Computer'
      },
      raspi4b4gb: {
        single: 'Raspberry Pi 4 Model B (4GB)',
        description: 'ARM based Single Board Computer'
      },
      raspi4b8gb: {
        single: 'Raspberry Pi 4 Model B (8GB)',
        description: 'ARM based Single Board Computer'
      }
    },
    esp32: {
      homepageLinkTitle: 'The homepage for ESP32',
      general: {
        single: 'ESP32',
        plural: 'ESP32',
        count: '{count} ESP32s | {count} ESP32 | {count} ESP32s',
      }
    },
    dell: {
      inspiron580: {
        single: 'Dell Inspiron 580',
        description: 'x86-64 desktop repurposed as a server'
      }
    },
    clevo: {
      p17sm: {
        single: 'Clevo P17SM',
        description: 'x86-64 laptop repurposed as a server'
      }
    },
    synology: {
      ds1618: {
        single: 'Synology DS1618+'
      },
      ds918: {
        single: 'Synology DS918+'
      }
    },
    ubiquiti: {
      usgpro4: {
        single: 'UniFi Security Gateway Pro 4',
        description: 'Network gateway and firewall'
      },
      usw24poe: {
        single: 'UniFi Switch 24-Port PoE',
        description: 'Power-over-Ethernet 24 Port network switch'
      },
      uswflexmini: {
        single: 'UniFi Switch Flex Mini',
        description: ''
      },
      uapacpro: {
        single: 'UniFi Access Point AC Pro',
        description: 'Access Point'
      }
    },
    enoc: {
      rack21u: {
        single: 'Enoc Server Rack',
        description: '21U enclosed server rack'
      }
    },
    apc: {
      srt3000xmi: {
        single: 'APC SRT3000XLI',
        description: '2U rack mount 3000VA UPS'
      },
      smt1000: {
        single: 'APC SMT1000',
        description: 'Standing 1000VA UPS'
      },
      dla1500rm2u: {
        single: 'APC DLA1500RM2U',
        description: '2U rack mount 1500VA UPS'
      }
    }
  },
  company: {
    alstom: {
      name: 'Alstom',
      homePageTitle: 'The home page for Alstom'
    },
  }
};

export const numbers = {

}

export const dateTime: IntlDateTimeFormat = {
  short: {
    year: 'numeric', month: 'short', day: 'numeric', 
  }
}
