import { onBeforeMount, onBeforeUnmount, ref } from 'vue';

export function usePreferDarkMode() {
  const mediaQueryList = ref<MediaQueryList | null>(null);
  const prefersDarkMode = ref<boolean>(false);

  const onMediaQueryListChanged = (event: MediaQueryListEvent): void => {
    prefersDarkMode.value = event.matches ?? false;
  }
  
  onBeforeMount(() => {
    mediaQueryList.value = window?.matchMedia('(prefers-color-scheme: dark)') ?? null;
  
    if (mediaQueryList.value) {
      prefersDarkMode.value = mediaQueryList.value.matches ?? false;
      mediaQueryList.value.addEventListener('change', onMediaQueryListChanged);
    }
  });
  
  onBeforeUnmount(() => {
    if (mediaQueryList.value) {
      mediaQueryList.value.removeEventListener('change', onMediaQueryListChanged);
    }
  })

  return {
    prefersDarkMode
  }
}
