import { computed } from 'vue';
import { RouteRecordName, RouteRecordRaw, useRouter, useRoute } from 'vue-router';

export interface VersionEntry {
  name: string;
  version: string;
  current: boolean;
  path: string;
}

export function isVersionEntry(entry: unknown): entry is VersionEntry {
  return entry !== undefined && entry !== null && typeof entry === 'object' &&
    'name' in entry && typeof entry.name === 'string' &&
    'version' in entry && typeof entry.version === 'string' &&
    'path' in entry && typeof entry.path === 'string' &&
    'current' in entry && typeof entry.current === 'boolean';
}

export function findProjectRoutes(
  routes: readonly RouteRecordRaw[],
  name: RouteRecordName | null | undefined,
  parentRoute: RouteRecordRaw | null = null,
  parentPath: string | null = null
): [RouteRecordRaw[], string] {
  if (name) {
    for (let route of routes) {
      if (route.name === name) {
        return [parentRoute?.children ?? [], parentPath ?? '/'];
      } else if (route?.children && route.children.length > 0) {
        return findProjectRoutes(
          route.children,
          name,
          route,
          parentPath ? `${parentPath}/${route.path}` : route.path
        );
      }
    }
  }

  return [[], '/'];
}

export function filterValidProjectRoutes(
  routes: readonly RouteRecordRaw[],
  name: RouteRecordName | null | undefined
) {
  const [projectRoutes, parentPath] = findProjectRoutes(routes, name);

  return projectRoutes
    .filter(route => route.meta?.version && !route.meta.hidden)
    .map(route => ({
      version: route.meta?.version?.toString() ?? null,
      name: route.name?.toString() ?? null,
      current: route.name === name,
      path: parentPath ? `${parentPath}/${route.path}` : route.path
    }))
    .filter(isVersionEntry)
    .sort(({ version: versionA },{ version: versionB }) => versionA.localeCompare(versionB));
}

export function useProjectVersions() {
  const currentRoute = useRoute();
  const router = useRouter();

  const knownProjectVersionRoutes = computed<VersionEntry[]>(() =>
    filterValidProjectRoutes(router.options.routes, currentRoute.name));

  return {
    knownProjectVersionRoutes
  };
}
