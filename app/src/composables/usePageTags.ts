import { computed } from 'vue';
import { useRoute } from 'vue-router';

export function usePageTags() {
  const route = useRoute();

  const pageTags = computed<string[]>(() => route.meta?.tags ?? []);

  return { pageTags };
}
